package com.example.eva2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

public class TerceraActivity extends AppCompatActivity {

    RadioGroup genderRadioGroup;
    Button backToSecondButton;
    Button gotoGaleria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tercera);

        genderRadioGroup = findViewById(R.id.genderRadioGroup);
        backToSecondButton = findViewById(R.id.backToSecondButton);
        gotoGaleria = findViewById(R.id.btnGoGaleria);

        backToSecondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = genderRadioGroup.getCheckedRadioButtonId();
                String gender = (selectedId == R.id.maleRadioButton) ? "Masculino" : "Femenino";

                Intent resultIntent = new Intent();
                resultIntent.putExtra("gender", gender);
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });

        gotoGaleria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent galeria = new Intent(TerceraActivity.this, GaleriaActivity.class);
                startActivity(galeria);
            }
        });
    }
}